#include <iostream>

using namespace std;

const int tamanho=10;

int vetor[tamanho];

void lerVetor(int vetor[],int tamanho){
    for (int i=0; i < tamanho; i++){
        cout << "Vetor[" << (i+1) << "]: ";
        cin >> vetor[i];
    }
}


int soma_comparativa(int vetor[],int tamanho){
    int resultado = 0;
    for (int i=0; i < tamanho; i++){
        resultado = resultado + vetor[i];
    }
    return resultado;
}

int soma(int vetor[],int inicio,int fim){
    int resultado = 0;
    for (int i = inicio; i < fim; i++){
        resultado = resultado + vetor[i];
    }
    return resultado;
}

int soma_recursiva(int vetor[], int inicio, int fim){
    int metade = (inicio + fim)/2;
    return soma(vetor,inicio,metade) + soma(vetor,metade,fim);
}

int main()
{
    lerVetor(vetor,tamanho);
    cout << "Resultado soma comparativa: " << soma_comparativa(vetor,tamanho) << endl;
    cout << "Resultado soma recursiva: " << soma_recursiva(vetor,0,tamanho) << endl;
    return 0;
}
